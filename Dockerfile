FROM wordpress:4-php7.1-fpm-alpine

ADD https://github.com/just-containers/s6-overlay/releases/download/v1.11.0.1/s6-overlay-amd64.tar.gz /tmp/
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C /

RUN apk add --update mysql mysql-client apache2 apache2-utils apache2-proxy nano && \
    mkdir -p /run/apache2/ && \
    rm -f /var/cache/apk/*

ADD tools /tools

RUN cd /tools && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    php composer.phar install


ADD root /
EXPOSE 80 443

ENTRYPOINT ["/init"]