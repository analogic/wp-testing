<?php

include "vendor/autoload.php";

use Goutte\Client;

$client = new Client();

$command = "/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'";
$localIP = exec($command);

$crawler = $client->request('GET', 'http://'.$localIP);

$form = $crawler->selectButton('Continue')->form();
$crawler = $client->submit($form, array('language' => 'en_GB'));

$crawler = $client->click($crawler->selectLink('Let’s go!')->link());

$form = $crawler->selectButton('Submit')->form();
$crawler = $client->submit($form, array('uname' => 'root', 'pwd' => '', 'dbhost' => '127.0.0.1'));

$crawler = $client->click($crawler->selectLink('Run the install')->link());

$form = $crawler->selectButton('Install WordPress')->form();
$crawler = $client->submit($form, array(
    'weblog_title' => 'root',
    'user_name' => 'test',
    'admin_password' => 'test',
    'admin_password2' => 'test',
    'pw_weak' => true,
    'admin_email' => 'test@test.cz',
));

$crawler = $client->click($crawler->selectLink('Log In')->link());
